var express = require('express')
const { auth } = require('express-openid-connect');

require('dotenv').config()

var router = require("./routes/router.js")



const app = express()
const config = {
	authRequired: false,
	auth0Logout: true,
	secret: process.env.SECRET,
	baseURL: process.env.APP_URL || process.env.BASEURL,
	clientID: process.env.CLIENTID,
	issuerBaseURL: process.env.ISSUERBASEURL
};

app.set("views", "views")
app.set("view engine", "ejs")
app.use(express.json())
app.use(express.urlencoded({ extended: true}))
app.use(express.static("public"))
app.use(auth(config))



app.use("/", router);

app.listen(process.env.PORT || 3000, () => {})