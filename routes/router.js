var express = require("express")
var router = express.Router()

var users

router.get("/", (req, res) => {
	(async () => {
		if (users == undefined) {
			users = []
		}
		console.log(users)
		res.render("index", {
			auth: req.oidc.isAuthenticated(),
			user: req.oidc.user,
			users: users,
		});
	}) ()
	
});

router.post('/addUser', async function (req, res, next) {
	(async () => {
		users = req.body.users
		res.redirect('/')
	}) ()
	
});



module.exports = router;